CPUS=$(shell nproc)

PROJECT_NAME=top_right_small_notification
PROJECT_VERSION=$(shell cat VERSION)

all:
	@echo 无事可做

run: pyuic
	@echo "是否存在 resource.qrc? 是否已经使用 make pyrcc 生成?" 
	@echo "正在运行."
	@/usr/bin/python3 $(PROJECT_NAME)

clean:
	/usr/bin/py3clean .

# 判断 pipreqs 是否存在，存在即可使用 pipreqs 进行导出 requirements.txt
PIPREQS="$(shell which pipreqs)"
ifneq ($(PIPREQS), "")
requirements-export:
	@echo "正在导出 requirements.txt"
	@pipreqs

requirements-export--force:
	@echo "正在导出 requirements.txt(使用 --force 参数覆盖)"
	@pipreqs --force

else
requirements:
	@echo "您是否未安装 pipreqs? 请使用以下命令来安装(否则请查看 Makefile):"
	@echo "    python3 -m pip install pipreqs"

endif

# 用于处理 pyuic 内容
statuswidget:
	test $(PROJECT_NAME)/widgets/statuswidget.ui && pyuic5 $(PROJECT_NAME)/widgets/statuswidget.ui > $(PROJECT_NAME)/widgets/statuswidget.py

mainwindow:
	test $(PROJECT_NAME)/mainwindow.ui && pyuic5 $(PROJECT_NAME)/mainwindow.ui > $(PROJECT_NAME)/mainwindow.py

test-mainwindow:
	test tests/mainwindow.ui && pyuic5 tests/mainwindow.ui > tests/mainwindow.py

pyuic: statuswidget mainwindow test-mainwindow 
	@echo "处理完成"
	
# 用于处理 pyrcc 内容
pyrcc:
	pyrcc5 resources.qrc -o $(PROJECT_NAME)/resources.py

################### 添加的其它操作(构建打包，安装?) ###################

# make dist 将进行构建 whl 文件与代码包
dist: dist-clean
	@echo "Build started"
	python3 ./setup.py sdist bdist_wheel

# make install 将安装到系统(建议使用sudo)
install:
	python3 ./setup.py install

# make install-whl 将安装 all 编译出的分发包
install-whl: 
	python3 -m pip install ./dist/$(PROJECT_NAME)-$(PROJECT_VERSION)-py3-none-any.whl

# make clean 将进行清空一些产生的文件
dist-clean: 
	rm -rf dist build $(PROJECT_NAME).egg-info
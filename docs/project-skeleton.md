# project 项目骨架预览

- 一个 python 项目基本主体结构

    ```
    .
    ├── assets
    │   └── spark.png
    ├── bin
    │   └── project -> ../project/__main__.py
    ├── docs
    │   └── project-skeleton.md
    ├── Makefile
    ├── project
    │   ├── base
    │   │   ├── baseMath.py
    │   │   ├── baseQt.py
    │   │   └── __init__.py
    │   ├── __init__.py
    │   ├── __main__.py
    │   ├── mainwindow.py
    │   ├── mainwindow.ui
    │   └── widgets
    │       ├── __init__.py
    │       └── picimage.py
    ├── README.md
    ├── requirements.txt
    ├── setup.py
    └── tests
        ├── __init__.py
        ├── mainwindow.py
        ├── mainwindow.ui
        └── testBaseQtAnimationMoveCenter.py

    7 directories, 20 files

    ```

- 其它说明

    - 来自 project 的说明

        ```python
        # 1. 项目'中的 project 目录'应为实际项目名称，而不是 src 或其它任何不为项目名称相关的命名
        #   例如有一个项目: PyqtDemo，项目名称为 project, 那么此项目的代码起点应该为 PyqtDemo/project
        #       之后 tests 中引用的包起点为 from project.xxx import *
        #
        # 2. 结构说明
        #   PyqtDemo/           # 项目，文件夹名称随意
        #       bin/
        #          project      # 文件，一般为链接形式链接到 project/__main__.py 
        #       ...
        #       project/        # 代码起点，由 from project 起始引入
        #           base/       # 项目的一些基础组成文件内容 由 from project.base 起始引入
        #               baseMath.py # 基本的数学计算函数
        #               baseQt.py   # 基本的 PyQt 扩展函数库
        #                   # 如果使用 from project.base import baseQt
        #                       使用时为 baseQt.move(a,b)
        #                   # 如果使用 from project.base.baseQt import move
        #                       使用时为 move(a,b)，但此种方式常常与继承的 QWidget 类提供的 move 产生重名
        #                       但在使用时的区别:
        #                           self.move(a,b)      来自继承的 QWidget 类提供的 move 函数
        #                           move(a,b)           来自 baseQt 类提供的 move 函数
        #                   # 如果使用 from project.base.baseQt import move as baseMove
        #                       使用时为 baseMove(a,b)
        #               ...
        #           __init__.py # 项目源代码初始化内容起点
        #           __main__.py # 项目主入口文件起点
        #                       # 如果项目类型主要以提供库函数则不需要此文件以及 bin 目录
        #
        #       tests/          # 项目的测试用例，任何项目类型都需要提供测试用例
        #           testBaseQtAnimationMoveCenter.py   # 例如使用 from project.base. 引入
        #       ...
        ```

    - 来自 tests 的说明

        任何测试用例都将符合以下形式

        ```python
        # 由于测试用例均会在 tests 目录，任何情况下默认只以此脚本所在目录为根目录(也就是 sys.path 第一个路径为此脚本所在目录的绝对路径)
        ['/home/zinface/Desktop/PyqtDemo/tests', ...] # 这种情况是无法在使用 from / import 时搜索到引入的项目名称及模块
                                                      # 另外 ... 中包含一些其它全局及用户目录的路径
                                                    ['/home/zinface/Desktop/PyqtDemo/tests', 
                                                        '/usr/lib/python37.zip', 
                                                        '/usr/lib/python3.7', 
                                                        '/usr/lib/python3.7/lib-dynload', 
                                                        '/home/zinface/.local/lib/python3.7/site-packages', 
                                                        '/usr/local/lib/python3.7/dist-packages', 
                                                        '/usr/local/lib/python3.7/dist-packages/law-0.1-py3.7.egg', 
                                                        '/usr/local/lib/python3.7/dist-packages/spark-0.1-py3.7.egg', 
                                                        '/usr/lib/python3/dist-packages']

        # 在添加了以下代码后， sys.path 将会增加用于在 from / import 时搜索项目名称或模块的路径
        ['/home/zinface/Desktop/PyqtDemo/tests', ... , '/home/zinface/Desktop/PyqtDemo', '/home/zinface/Desktop/PyqtDemo/tests']

        import sys
        # 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
        sys.path.append(os.path.abspath("."))
        # 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
        sys.path.append(os.path.abspath(".."))

        from project.base.baseQt import *
        ```

    - 来自运行测试用例期间可能会出现 `ModuleNotFoundError: No module named 'project.xxx'` 的问题

        > 与来自 tests 说明类似，但进行了详细的补充说明

        ```shell
        # 由于此类测试用例脚本均默认以 from project.xxx 为引入项目模块，所以应该有下两种形式解决其产生的引用问题

        # 1. 运行此脚本的解释器运行位置(工作目录)必须为 project 目录所在目录(确保在项目目录下, 而不是项目名称(project)目录)
            sys.path.append(os.path.abspath("."))

        # 2. 在 Code Runner 插件中运行脚本时，会自动进行 cd 此脚本目录，并由插件配置的执行程序运行被执行的文件
            # 例如 Code Runer 插件将会激活'Code - tests'名称的终端，并在此处执行以下命令
                cd "/home/zinface/Desktop/PyqtDemo/tests"
                python3 -u "/home/zinface/Desktop/PyqtDemo/tests/testBaseQtAnimationMoveCenter.py"
            # 此时 sys.path 中仅有路径为此脚本的所在目录路径，并未加入相对于 project 目录所在目录的绝对路径，使用 Code Runner 将出现以下错误
                Traceback (most recent call last):
                File "/home/zinface/Desktop/PyqtDemo/tests/testBaseQtAnimationMoveCenter.py", line 12, in <module>
                    from project.base.baseQt import *
                ModuleNotFoundError: No module named 'project'

        # 3. 此脚本内应进行处理 sys.path 增加相对于 project 目录所在目录的绝对路径
            # 此脚本在 tests 目录，那么脚本的上一级目录就是 project 目录所在目录
            sys.path.append(os.path.abspath(".."))
        ```

        用一种最传统的做法

        ```shell
        # 在脚本内增加项目目录的路径(例如在桌面上有一个 PyqtDemo 项目，而项目中有 project 与 tests 目录)
        #
        #   PyqtDemo/
        #       ...
        #       project/
        #           base/
        #              ...
        #       tests/
        #           testBaseQt.py
        #       ...
        #   增加路径的方式举例：
        #       举例 Linux:   sys.path.append("/home/zinface/Desktop/PyqtDemo")
        #       举例 Windows: sys.path.append("C:\Users\Administrator\Desktop\PyqtDemo")
        ```
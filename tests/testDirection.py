import os
import sys

# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 

from top_right_small_notification.widgets.StatusWidget import StatusWidget

if __name__ == '__main__':
    
    a = StatusWidget.EnterLeaveDuretion.BottomOut
    
    print(a == StatusWidget.EnterLeaveDuretion.BottomIn)
    
    print(a)
#!/usr/bin/env python3

import sys,os

sys.path.append(os.path.abspath(".")) 

from top_right_small_notification.base.baseQt import *
# from widgets.StatusWidget import StatusWidget
from top_right_small_notification.widgets import *

from top_right_small_notification.mainwindow import Ui_MainWindow

# 引入由 pyrcc5 编译的 resources.qrc 生成的 resource.py 内容
# 虽然被 IDE 或 Python 语言服务器报告为未使用(unused)，但因为此模块内部资源是自加载
# 此模块在导入时会自动运行 qInitResources() 函数加载二进制资源(反正是自动的)
import top_right_small_notification.resources

from PyQt5.QtGui import QPixmap

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.ui.pushButton.clicked.connect(self.pushButton_clicked)
        self.ui.pushButton_2.clicked.connect(self.pushButton_2_clicked)
        self.ui.pushButton_3.clicked.connect(self.pushButton_3_clicked)
        self.ui.pushButton_4.clicked.connect(self.pushButton_4_clicked)
        
        # 将此部分直接在 statuswidget 控件中实现也是个不错想法
        self.ui.widget.setIcon(QPixmap(":/icon/assets/icon/iconfinder_4591899_animal_carnivore_cartoon_cat_house pet_icon.svg"))

    def pushButton_clicked(self):
        self.ui.widget.setShowMessage("提示","这是上进右出");
        self.ui.widget.setDirection(StatusWidget.EnterLeaveDuretion.TopIn, StatusWidget.EnterLeaveDuretion.RightOut)
        self.ui.widget.start()
        
    def pushButton_2_clicked(self):
        self.ui.widget.setShowMessage("提示","这是右进上出");
        self.ui.widget.setDirection(StatusWidget.EnterLeaveDuretion.RightIn, StatusWidget.EnterLeaveDuretion.TopOut)
        self.ui.widget.start()
        
    def pushButton_3_clicked(self):
        self.ui.widget.setShowMessage("提示","这是下进右出");
        self.ui.widget.setDirection(StatusWidget.EnterLeaveDuretion.BottomIn, StatusWidget.EnterLeaveDuretion.RightOut)
        self.ui.widget.start()
    
    def pushButton_4_clicked(self):
        self.ui.widget.setShowMessage("提示","这是右进下出");
        self.ui.widget.setDirection(StatusWidget.EnterLeaveDuretion.RightIn, StatusWidget.EnterLeaveDuretion.BottomOut)
        self.ui.widget.start()

if __name__ == '__main__':
    os.putenv(b"QT_QPA_PLATFORM", b"")
    
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()

    # moveWidgetCenterInDesktopWidget(window)
    animationMoveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())


# 在此处定义可由外部使用 from <项目名>.widgets import (xxx) 方式可用的内容
# 一般情况下 xxx 为 *，例如:
    # from <项目名>.widgets import *

# 1. 在此处使用'相对位置'定义
from .StatusWidget import StatusWidget
# 2. 在此处使用'绝对位置'定义
from top_right_small_notification.widgets.StatusWidget import StatusWidget

# 关于位置定义，两者无任何实际差别(但有微妙不同，以项目名字为 project 字面意义，则不易于直接拷贝此 widgets 目录进行移植到其它项目中)

# NOTE: 如果在此处定义由在 'widgets' 中包含可使用的默认组件(或者叫在 widgets 模块下的所有库)
#   则可以在其它位置使用 from widgets import * 来导入此处定义的所有内容
#   而无需使用类似以下内容:
#       from widgets.StatusWidget import StatusWidget
#       from widgets.Xxx import Xxx
#       from widgets.Yyy import Yyy
#       from widgets.Zzz import Zzz
#       from widgets.Xyy import Xyy
#       from widgets.Yyy import Yyy
#       from widgets.Zzz import Zzz
#       ... 如果 widgets 模块下有很多很多库呢(建议使用简略化代码)


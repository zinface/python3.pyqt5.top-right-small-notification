from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtNetwork import *


# 在此处定义可由外部使用 from <项目名>.base import (xxx) 方式可用的内容
# 一般情况下 xxx 为 *，例如:
    # from <项目名>.base import *

# 1. 在此处使用'相对位置'定义
from .baseMath import * 
from .baseQt import *
# 2. 在此处使用'绝对位置'定义
# from top_right_small_notification.base.baseMath import *
# from top_right_small_notification.base.baseQt import *

# 关于位置定义，两者无任何实际差别(但有微妙不同，以项目名字为 project 字面意义，则不易于直接拷贝此 base 目录进行移植到其它项目中)

# NOTE: 如果在此处定义由在 'base' 中包含可使用的默认组件(或者叫在 base 模块下的所有库)
#   则可以在其它位置使用 from base import * 来导入此处定义的所有内容
#   而无需使用类似以下内容:
#       from base.StatusWidget import StatusWidget
#       from base.Xxx import Xxx
#       from base.Yyy import Yyy
#       from base.Zzz import Zzz
#       from base.Xyy import Xyy
#       from base.Yyy import Yyy
#       from base.Zzz import Zzz
#       ... 如果 base 模块下有很多很多库呢(建议使用简略化代码)


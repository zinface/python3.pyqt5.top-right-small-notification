
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtNetwork import *


"""
基本的控件移动函数的 Helper 巨集
"""

def moveWidgetCenterInWidget(widget, parent):
    """从父控件几何中计算 widget 应该移动的中间位置"""
    parentWidth = parent.geometry().width()
    parentHeight = parent.geometry().height()

    # 2. 计算
    width = baseMath.mathDivide(parentWidth - widget.width(), 2)
    height = baseMath.mathDivide(parentHeight - widget.height(), 2)

    widget.move(width, height)

# 可用的函数，已经实现
from . import baseMath
def moveWidgetCenterInDesktopWidget(widget):
    """移动到 DesktopWidget 的中间位置
    效果:
        直接显示在屏幕中间
    """
    moveWidgetCenterInWidget(widget, parent=QDesktopWidget())
    
    # so 为什么不用这个函数直接完成？因为显得更nb
    # widget.move(QDesktopWidget().rect().center() - widget.rect().center())


# 可用的函数，已经实现功能，但未实现 Primary Screen 的功能
def movePrimaryDesktopCenter(widget):
    """将 widget 移动到主屏幕中间
    目前未实现此部分
    """
    widget.move(QDesktopWidget().rect().center() - widget.rect().center())

# 无用的函数，未实现所需要的功能
# def moveDesktopCenter(widget):
#     desktop = QDesktopWidget()
#     desktop.move(widget.rect().center() - desktop.rect().center())
#     return desktop


def animationMoveWidgetCenterInWidget(widget: QWidget, parent: QWidget, duration: int = 1000):
    """以某个控件为参考对象使用居中动画"""
    # 1. 实例化动画类，操作的对象，操作的对象属性，父对象
    animation = QPropertyAnimation()
    animation.setTargetObject(widget)
    animation.setPropertyName(b'pos')
    animation.setParent(widget)
    
    # so 为什么不用这个函数直接完成？因为显得更详细?
    animation = QPropertyAnimation(widget, b'pos', widget)

    # 2. 设置属性值的启始位置(原位)，与结束位置(传入控件的中心)
    # 结束位置：通过原位置的中心点与目标位置的中心点，可得出
        # RECT 拥有左上右上左下右下四个坐标
    animation.setStartValue(widget.pos())
    animation.setEndValue(parent.rect().center() - widget.rect().center())
    
    
    # 3. 设置缓和曲线，使用某种加速与减速的方式进行实现动画效果
    # 二次(t^2)函数的松弛曲线:加速到一半，然后减速。
    animation.setEasingCurve(QEasingCurve.Type.InOutQuart)
    # 二次(t^2)函数的松弛曲线:减速到一半，然后加速。
    # animation.setEasingCurve(QEasingCurve.Type.OutInQuart)
    
    # 4. 设置动画效果所用时间，可认为在该时间内完成动画
    animation.setDuration(duration)

    # 5. 启动动画效果
    animation.start()
    
def animationMoveWidgetCenterInDesktopWidget(widget):
    """以DesktopWidget控件为参考对象使用居中动画
    效果:
        可以将这个 widget 移动到屏幕中间(动画效果)
    """
    animationMoveWidgetCenterInWidget(widget, parent=QDesktopWidget())

def loaderQss(path):
    pass

def loaderFont(path):
    pass
